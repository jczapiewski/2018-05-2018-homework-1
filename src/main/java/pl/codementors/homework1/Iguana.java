package pl.codementors.homework1;

public class Iguana extends Lizard implements Herbivore {

    public Iguana(String name, int age, String scalesColour) {
        super(name, age, scalesColour);
    }

    public void hiss() {
        System.out.println(getName() + " syczy");
    }
    public void eat() {
        System.out.println(getName() + " je");

    }

    public void eatingPlants() {
        System.out.println(getName() + " je roślinę");
    }
}
