package pl.codementors.homework1;

public class Parrot extends Bird implements Herbivore {

    public Parrot(String name, int age, String plumageColour) {
        super(name, age, plumageColour);
    }

    public void tweet() {
        System.out.println(getName() + " ćwierka");
    }

    public void eat() {
        System.out.println(getName() + " je");
    }

    public void eatingPlants() {
        System.out.println(getName() + " je roślinę");

    }
}
