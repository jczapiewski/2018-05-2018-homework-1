package pl.codementors.homework1;

public abstract class Lizard extends Animal {

    private String scalesColour;

    public String getScalesColour() {
        return scalesColour;
    }

    public Lizard(String name, int age, String scalesColour) {
        super(name, age);
        this.scalesColour = scalesColour;
    }
}

