package pl.codementors.homework1;

import java.io.Serializable;

public abstract class Animal implements Serializable {

    private String name;
    private int age;

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public Animal(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public abstract void eat ();
}
