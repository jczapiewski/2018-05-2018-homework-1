package pl.codementors.homework1;

public abstract class Mammal extends Animal {

    private String furColour;

    public String getFurColour() {
        return furColour;
    }

    public Mammal(String name, int age, String furColour) {
        super(name, age);
        this.furColour = furColour;
    }
}
