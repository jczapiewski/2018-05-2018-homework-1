package pl.codementors.homework1;

public class Wolf extends Mammal implements Carnivore {

    public Wolf(String name, int age, String furColour) {
        super(name, age, furColour);
    }

    public void howl() {
        System.out.println(getName() + " wyje");
    }

    @Override
    public void eat() {
        System.out.println(getName() + " je");
    }

    @Override
    public void eatingMeat() {
        System.out.println(getName() + " je mięso");

    }
}
