package pl.codementors.homework1;

import java.io.*;

/**
 * Main classfor the application
 * @author Kuba
 */

public class Main {

        public static void main(String[] args) {

        System.out.println("Witaj w ZOO!");

        String fileReadName = "test";
        String fileWriteName = "test2";
        String binFileName = "test3.bin";

        //Reading array of animals from file
        Animal[] animals = readAnimals(fileReadName);
        //Printing all of animals to console
        printAnimalsArr(animals);
        //Writing all animals to file
        saveAnimals(fileWriteName, animals);
        //Feeding all animalsm carnivores and herbivores
        fedAllAnimals(animals);
        fedAllCarnivores(animals);
        fedAllHerbivores(animals);
        //Methods that cause sounds
        howling(animals);
        hissing(animals);
        tweeting(animals);
        //Writing to binary file
        writeToBin(binFileName, animals);
        //Reading from binary file
        Animal[] animalsFromBin = readFromBin(binFileName);
        //Printing all of animals from binary file to console
        printAnimalsArr(animalsFromBin);
    }

    /**
     * Reads arrray of animals from file
     * @param fileName Text file containing in first line number of animals, and then typ, name, age and color of animal
     * @return Array of animals
     */
    private static Animal[] readAnimals(String fileName) {
        File file = new File(fileName);
        Animal[] animals = null;

        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            int size = Integer.parseInt(br.readLine());
            animals = new Animal[size];

                for (int i = 0; i < size; i++) {
                    String type = br.readLine();
                    String name = br.readLine();
                    int age = Integer.parseInt(br.readLine());
                    String color = br.readLine();

                    if (type.equals("Wolf")) {
                        animals[i] = new Wolf(name, age, color);
                    } else if (type.equals("Iguana")) {
                        animals[i] = new Iguana(name, age, color);
                    } else if (type.equals("Parrot")) {
                        animals[i] = new Parrot(name, age, color);
                    }
                } return animals;
        } catch (IOException ex) {
            System.err.println(ex);
        } return null;
    }

    /**
     * Method that writes array of animals to file
     * @param fileName File name
     * @param animals Array of animals
     */
    private static void saveAnimals(String fileName, Animal[] animals) {
        //fileName = "test2";
        File file = new File(fileName);
        try (BufferedWriter br = new BufferedWriter(new FileWriter(file))) {
            br.write("" + animals.length + "\n");
            for (int i = 0; i < animals.length; i++) {
                if (animals[i] instanceof Wolf) {
                    br.write("Wolf\n");
                    br.write(animals[i].getName() + "\n");
                    br.write(animals[i].getAge() + "\n");
                    br.write(((Wolf)animals[i]).getFurColour() + "\n");
                } else if (animals[i] instanceof Iguana) {
                    br.write("Iguana\n");
                    br.write(animals[i].getName() + "\n");
                    br.write(animals[i].getAge() + "\n");
                    br.write("" + ((Iguana) animals[i]).getScalesColour() + "\n");
                } else if (animals[i] instanceof Parrot) {
                    br.write("Parrot\n");
                    br.write(animals[i].getName() + "\n");
                    br.write(animals[i].getAge() + "\n");
                    br.write(((Parrot)animals[i]).getPlumageColour() + "\n");
                }
            }

        } catch (IOException ex) {
            System.err.println(ex);
        } finally {
            System.out.println("OK");
        }
    }

    /**
     * Method that causes all of animals are printed to the console
     * @param animals Array of animals
     */
    private static void printAnimalsArr(Animal[] animals) {
        System.out.println(animals.length + "\n");
        for (int i = 0; i < animals.length; i++) {
            if (animals[i] instanceof Wolf) {
                System.out.println("Wilk");
                System.out.println("Imię: " + animals[i].getName());
                System.out.println("Wiek: " + animals[i].getAge() + " lat");
                System.out.println("Kolor futra: " + ((Wolf)animals[i]).getFurColour());
            } else if (animals[i] instanceof Iguana) {
                System.out.println("Iguana");
                System.out.println("Imie " + animals[i].getName());
                System.out.println("Wiek: " + animals[i].getAge());
                System.out.println("Kolor łusek: " + ((Iguana)animals[i]).getScalesColour());
            } else if (animals[i] instanceof Parrot) {
                System.out.println("Papuga");
                System.out.println("Imię: " + animals[i].getName());
                System.out.println("Wiek: " + animals[i].getAge());
                System.out.println("Kolor futra: " + ((Parrot)animals[i]).getPlumageColour());
            } else {
                System.out.println("Coś sie zepsuło");
            }
        }
    }

    /**
     * Method that causes all of animals are eating
     * @param animals Array of animals
     */

    private static void fedAllAnimals(Animal[] animals) {
            for (int i = 0; i < animals.length; i++) {
                animals[i].eat();
            }
        System.out.println();
    }

    /**
     * Method that fed all of carnivores
     * @param animals Array of animals
     */
    private static void fedAllCarnivores(Animal[] animals) {
            for (int i = 0; i < animals.length; i++) {
                if (animals[i] instanceof Carnivore) {
                    ((Carnivore)animals[i]).eatingMeat();
                }
            }
        System.out.println();
    }

    /**
     * Method that fed all of herbivores
     * @param animals Array of animals
     */
    private static void fedAllHerbivores(Animal[] animals) {
        for (int i = 0; i < animals.length; i++) {
            if (animals[i] instanceof Herbivore) {
                ((Herbivore)animals[i]).eatingPlants();
            }
        }
        System.out.println();
    }

    /**
     * Method that causes all of wolfs are howling
     * @param animals Array of animals
     */
    private static void howling(Animal[] animals) {
            for (Animal animal : animals) {
                if (animal instanceof Wolf) {
                    ((Wolf)animal).howl();
                }
            }
        System.out.println();
    }

    /**
     * Method that causes all of Iguanas are hissing
     * @param animals Array of animals
     */
    private static void hissing(Animal[] animals) {
        for (Animal animal : animals) {
            if (animal instanceof Iguana) {
                ((Iguana)animal).hiss();
            }
        }
        System.out.println();
    }

    /**
     * Method that causes all of Parrots are tweeting
     * @param animals Array of animals
     */
    private static void tweeting(Animal[] animals) {
        for (Animal animal : animals) {
            if (animal instanceof Parrot) {
                ((Parrot)animal).tweet();
            }
        }
        System.out.println();
    }

    /**
     * Method that writes to binary file array of animals
     * @param fileName File name
     * @param animals Array of animals
     */
    private static void writeToBin (String fileName, Animal[] animals) {
            File file = new File(fileName);
            try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {
                oos.writeObject(animals);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
    }

    /**
     * Method that reads from binary file array of animals
     * @param fileName File name
     * @return Array of animals
     */
    private static Animal[] readFromBin(String fileName) {
            File file = new File(fileName);
            Animal[] animals = null;
            try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
                animals = (Animal[])ois.readObject();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } return animals;
    }
}
