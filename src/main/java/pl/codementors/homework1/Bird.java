package pl.codementors.homework1;

public abstract class Bird extends Animal {

    private String plumageColour;

    public String getPlumageColour() {
        return plumageColour;
    }

    public Bird(String name, int age, String plumageColour) {
        super(name, age);
        this.plumageColour = plumageColour;
    }
}
